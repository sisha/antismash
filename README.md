antiSMASH - the antibiotics and Secondary Metabolite Analysis SHell
===================================================================

[![Build Status](https://bitbucket.drone.secondarymetabolites.org/api/badges/antismash/antismash/status.svg)](https://bitbucket.drone.secondarymetabolites.org/antismash/antismash)
[![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat-square)](http://bioconda.github.io/recipes/antismash/README.html)

antiSMASH allows the rapid genome-wide identification, annotation and analysis
of secondary metabolite biosynthesis gene clusters in bacterial and fungal
genomes. It integrates and cross-links with a large number of in silico
secondary metabolite analysis tools that have been published earlier.

External tools
--------------

antiSMASH is powered by several open source tools: NCBI BLAST+,HMMer 3, Muscle
3, Glimmer 3, FastTree, TreeGraph 2, Indigo-depict, PySVG and JQuery SVG.


Development & Funding
---------------------

The development of antiSMASH was started as a collaboration of the Department
of Microbial Physiology and Groningen Bioinformatics Centre of the University
of Groningen, the Department of Microbiology of the University of Tübingen, and
the Department of Bioengineering and Therapeutic Sciences at the University of
California, San Francisco.
With the move of the PIs and developers, development continues now at the
Manchester Institute of Biotechnology, the Bioinformatics Group at Wageningen
University and The Novo Nordisk Foundation Center for Biosustainability in
Hørsholm.

antiSMASH development was/is supported by the GenBiotics program of the Dutch Technology
Foundation (STW), which is the applied-science division of The Netherlands
Organisation for Scientific Research (NWO) and the Technology Programme of the
Ministry of Economic Affairs (grant STW 10463), GenBioCom program
of the German Ministry of Education and Research (BMBF) grant 0315585A, the
German Center for Infection Research (DZIF) and the Novo Nordisk Foundation.

Publications
------------

Kai Blin, Marnix H. Medema, Daniyal Kazempour, Michael A. Fischbach, Rainer
Breitling, Eriko Takano, & Tilmann Weber (2013): antiSMASH 2.0 — a versatile
platform for genome mining of secondary metabolite producers. Nucleic Acids
Research 41: W204-W212.

Marnix H. Medema, Kai Blin, Peter Cimermancic, Victor de Jager, Piotr
Zakrzewski, Michael A. Fischbach, Tilmann Weber, Rainer Breitling & Eriko
Takano (2011). antiSMASH: Rapid identification, annotation and analysis of
secondary metabolite biosynthesis gene clusters. Nucleic Acids Research 39:
W339-W346.


Installation
------------

Unfortunately, antiSMASH is a bit complicated to install from source, and we
recommend using one of the pre-built installers from
[our download page](http://antismash.secondarymetabolites.org/download)
instead.

###Manual installation on Linux

First, make sure you have the following antiSMASH dependencies installed:

- [diamond](https://github.com/bbuchfink/diamond) (version 0.8.36 tested)
- [fasttree](http://www.microbesonline.org/fasttree/#Install) (version 2.1.7 tested)
- [glimmer](https://ccb.jhu.edu/software/glimmer/) (version 3.02 tested)
- [GlimmerHMM](https://ccb.jhu.edu/software/glimmerhmm/) (version 3.0.4 tested)
- [hmmer2](http://hmmer.janelia.org/download.html) (version 2.3.2 tested, append a 2 to all hmmer2 executables to avoid conflict with hmmer3 executable names, like hmmalign -> hmmalign2)
- [hmmer3](http://hmmer.janelia.org/download.html) (version 3.1b2 tested)
- [mafft](http://mafft.cbrc.jp/alignment/software/) (version 7.271 tested)
- [meme](http://meme-suite.org/meme-software/) (version 4.11.2 tested. **Version 4.11.4 changes output file formats, so don't use that.**)
- [muscle](http://www.drive5.com/muscle/downloads.htm) (version 3.8.31 tested)
- [NCBI blast+](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/) (version 2.2.31 tested)
- [prodigal](http://prodigal.ornl.gov/) (version 2.6.1 tested)
- [xml](http://xmlsoft.org) development headers (version 2.9.1 tested)
- [xz](http://tukaani.org/xz/) development headers (version 5.1.1 tested)
- python (version 2.7.9 tested, any python 2.x >= python 2.6 should work)
- python-virtualenv (not needed, but highly recommended)

Then, create a python virtualenv for installing the antiSMASH python
dependencies. This is not required, but highly recommended.

```bash
virtualenv as3
source as3/bin/activate
```

All the python dependencies are listed in `requirements.txt`, you can grab them
all and install them with a simple command:

```bash
pip install -r requirements.txt
```

Last but not least, run `download_databases.py` to grab and prepare the
databases:

```bash
python download_databases.py
```
