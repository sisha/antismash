import unittest
from helperlibs.bio import seqio
from antismash import utils
from antismash.config import set_config
from antismash.specific_modules.lassopeptides import specific_analysis
from antismash.specific_modules.lassopeptides import html_output as h
from antismash.specific_modules.lassopeptides import check_prereqs

check_prereqs()  # ensure fimo detected

class IntegrationLasso(unittest.TestCase):
    def setUp(self):
        from argparse import Namespace
        conf = Namespace()
        conf.cpus = 1
        conf.without_fimo = False
        set_config(conf)

    def tearDown(self):
        set_config(None)

    def integration_astexin1(self):
        "Test lassopeptides prediction for astexin-1"
        rec = seqio.read(utils.get_full_path(__file__, 'astex1.gbk'))
        self.assertEqual(2443, len(rec.features))

        specific_analysis(rec, None)
        self.assertEqual(2445, len(rec.features))
        peptides = h._find_core_peptides(utils.get_cluster_by_nr(rec, 2), rec)
        self.assertEqual(1, len(peptides))
        peptide = peptides[0]
        leaders = h._find_leader_peptides(utils.get_cluster_by_nr(rec, 2), rec)
        self.assertEqual(1, len(leaders))
        leader = leaders[0]
        
        # values considering tail cut
        self.assertAlmostEqual(2093.0, h._get_monoisotopic_mass(peptide))
        self.assertAlmostEqual(2094.2, h._get_molecular_weight(peptide))
        self.assertEqual(0, h._get_number_bridges(peptide))
        self.assertEqual("MHTPIISETVQPKTAGLIVLGKASAETR", h._get_leader_peptide_sequence(leader))
        self.assertEqual("GLSQGVEPDIGQTYFEESR", h._get_core_peptide_sequence(peptide))
        self.assertEqual('Class II', h._get_core_peptide_class(peptide))
        self.assertEqual('GLSQGVEPD', h._get_macrolactam(peptide))
        self.assertEqual('INQD', h._get_tail_cut(peptide))
        self.assertEqual(2563.2, h._get_monoisotopic_mass_uncut(peptide))
        self.assertEqual(2564.7, h._get_molecular_weight_uncut(peptide))     

    def integration_burhizin(self):
        "Test lassopeptide prediction for burhizin"
        rec = seqio.read(utils.get_full_path(__file__, 'burhizin.gbk'))
        self.assertEqual(3498, len(rec.features))

        specific_analysis(rec, None)
        self.assertEqual(3500, len(rec.features))
        peptides = h._find_core_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(peptides))
        peptide = peptides[0]


        leaders = h._find_leader_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(leaders))
        leader = leaders[0]

        self.assertAlmostEqual(1846.9, h._get_monoisotopic_mass(peptide))
        self.assertAlmostEqual(1847.9, h._get_molecular_weight(peptide))
        self.assertEqual(0, h._get_number_bridges(peptide))
        self.assertEqual("MNKQQQESGLLLAEESLMELCASSETL", h._get_leader_peptide_sequence(leader))
        self.assertEqual("GGAGQYKEVEAGRWSDR", h._get_core_peptide_sequence(peptide))
        self.assertEqual('Class II', h._get_core_peptide_class(peptide))
        self.assertEqual('GGAGQYKE', h._get_macrolactam(peptide))
        self.assertEqual('IDSDDE', h._get_tail_cut(peptide))
        self.assertEqual(2521.1, h._get_monoisotopic_mass_uncut(peptide))
        self.assertEqual(2522.5, h._get_molecular_weight_uncut(peptide))  

    def integration_SSV2083(self):
        "Test lassopeptide prediction for SSV-2083"
        rec = seqio.read(utils.get_full_path(__file__, 'SSV2083.gbk'))
        self.assertEqual(25, len(rec.features))

        specific_analysis(rec, None)
        self.assertEqual(27, len(rec.features))
        peptides = h._find_core_peptides(utils.get_cluster_by_nr(rec, 22), rec)
        self.assertEqual(1, len(peptides))
        peptide = peptides[0]
        leaders = h._find_leader_peptides(utils.get_cluster_by_nr(rec, 22), rec)
        self.assertEqual(1, len(leaders))
        leader = leaders[0]
       
        self.assertAlmostEqual(2086.8, h._get_monoisotopic_mass(peptide))
        self.assertAlmostEqual(2088.4, h._get_molecular_weight(peptide))
        self.assertEqual(2, h._get_number_bridges(peptide))
        self.assertEqual("VLISTTNGQGTPMTSTDELYEAPELIEIGDYAELTR", h._get_leader_peptide_sequence(leader))
        self.assertEqual("CVWGGDCTDFLGCGTAWICV", h._get_core_peptide_sequence(peptide))
        self.assertEqual('Class I', h._get_core_peptide_class(peptide))
