# set fileencoding: utf-8
import unittest

from antismash.specific_modules.nrpspks import nrps_utils


class TestNRPSUtils(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_resolve_predicat_domain_specificity(self):
        test_lines = [
            ("BGC0000400_AGM16413.1_1312_mod2", 'lys'),
            ("Q6YK41_A1", 'asn'),
            ("Q8G983_A1-6-", 'ser'),
            ("BGC0000376_BAP16689.1_223_mod1", '6cl-4hydroxyindole-3-carboxylic'),
            ("Q83VS0_A5-15-", 'athr|thr'),
            ("simA_mod5", 'bmt'),
        ]
        for val, expected in test_lines:
            res = nrps_utils.resolve_predicat_domain_specificity(val)
            self.assertEqual(expected, res)

    def test_find_full_id_line(self):
        test_lines = [
            ("BGC0000400_AGM16413.1_1312_mod2", "BGC0000400_AGM16413.1_1312_mod2_lys"),
            ("Q6YK41_A1", "Q6YK41_A1_asn"),
            ("Q8G983_A1-6-", "Q8G983_A1(6)_ser"),
            ("BGC0000376_BAP16689.1_223_mod1", "BGC0000376_BAP16689.1_223_mod1_6cl-4hydroxyindole-3-carboxylic"),
            ("Q83VS0_A5-15-", "Q83VS0_A5(15)_athr|thr"),
            ("simA_mod5", "simA_mod5_bmt"),
            ("nonexistent_line", "no_match_N/A"),
        ]
        for val, expected in test_lines:
            res = nrps_utils.find_full_id_line(val)
            self.assertEqual(expected, res)

    def test_get_specificity_from_id_line(self):
        test_lines = [
            ("BGC0000400_AGM16413.1_1312_mod2_lys", 'lys'),
            ("Q6YK41_A1_asn", 'asn'),
            ("Q8G983_A1(6)_ser", 'ser'),
            ("BGC0000376_BAP16689.1_223_mod1_6cl-4hydroxyindole-3-carboxylic", '6cl-4hydroxyindole-3-carboxylic'),
            ("Q83VS0_A5(15)_athr|thr", 'athr|thr'),
            ("simA_mod5_bmt", 'bmt'),
        ]
        for val, expected in test_lines:
            res = nrps_utils.get_specificity_from_id_line(val)
            self.assertEqual(expected, res)

    def test_load_id_lines(self):
        id_lines = nrps_utils.load_id_lines()
        self.assertEqual(928, len(id_lines))
        for i, line in enumerate(id_lines):
            assert not line.startswith(">"), "line {i} starts with >: {line!r}".format(i=i, line=line)
